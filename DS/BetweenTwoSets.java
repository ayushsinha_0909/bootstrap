import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the getTotalX function below.
     */
    static int getTotalX(int[] a, int[] b) {
        
        int an = a.length;
        int bn = b.length;
        int max = 0;
        int flag = 0;
        int min =1000000;
        int count = 0;
        for(int i = 0; i < an; i++){
            if(a[i]>max){
                max = a[i];
            }
        }
        for(int i = 0; i < bn; i++){
            if(b[i]<min){
                min = b[i];
            }
        }
        for(int i = max; i <= min; i+=max){
            for(int j = 0; j < an; j++){
                if(i % a[j] != 0){
                    flag = 1;
                }
            }
            for(int k = 0; k < bn; k++){
                if(b[k] % i != 0){
                    flag = 1;
                }
            }
            if(flag == 0){
                count++;
            }
            else
                flag = 0;
        }
    return count;
    }
     
     
    
    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nm = scan.nextLine().split(" ");

        int n = Integer.parseInt(nm[0].trim());

        int m = Integer.parseInt(nm[1].trim());

        int[] a = new int[n];

        String[] aItems = scan.nextLine().split(" ");

        for (int aItr = 0; aItr < n; aItr++) {
            int aItem = Integer.parseInt(aItems[aItr].trim());
            a[aItr] = aItem;
        }

        int[] b = new int[m];

        String[] bItems = scan.nextLine().split(" ");

        for (int bItr = 0; bItr < m; bItr++) {
            int bItem = Integer.parseInt(bItems[bItr].trim());
            b[bItr] = bItem;
        }

        int total = getTotalX(a, b);

        bw.write(String.valueOf(total));
        bw.newLine();

        bw.close();
    }
}
