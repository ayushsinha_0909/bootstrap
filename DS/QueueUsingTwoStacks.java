import java.io.*;
import java.util.*;

public class Solution {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Stack<Integer> stack1 = new Stack<Integer>();
        Stack<Integer> stack2 = new Stack<Integer>();
        int query = sc.nextInt();
        
        int temp; int number = 0;
       for(int i = 0; i < query; i++) {
            temp = sc.nextInt();
            switch(temp) {
                
                case 1: 
                    number = sc.nextInt();
                    stack1.push(number);
                    break;
                
                case 2:
                    if(stack2.isEmpty()) {
                        while(!stack1.isEmpty()) {
                            stack2.push(stack1.pop());
                        }
                    }
                    stack2.pop();
                    break;
                
                case 3:
                    if(stack2.isEmpty()) {
                        while(!stack1.isEmpty()) {
                            stack2.push(stack1.pop());
                        }
                    }
                    System.out.println(stack2.peek());
                    break;
            }
        } 
    }
}
